import { EmonAppPage } from './app.po';

describe('emon-app App', () => {
  let page: EmonAppPage;

  beforeEach(() => {
    page = new EmonAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
