
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { UpdateService } from './services/update.service';
import { EmonClientService } from './services/emon.client.service';
import { LoginComponent } from './components/login.component';
import { GraphsComponent } from './components/graphs.component';
import { PowerGraphComponent } from './components/power.graph.component';
import { HomeComponent } from './components/home.component';
import { RegisterComponent } from './components/register.component';
import { RegisterSuccessComponent } from './components/register.success.component';
import { EmailVerifiedComponent } from './components/email.verified.component';
import { ResetPasswordComponent } from './components/reset.password.component';
import { RequestPasswordComponent } from './components/request.password.component';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AlertComponent } from './directives/alert.component';
import { AuthGuard } from './guards/auth.guard';
import { AlertService } from './services/alert.service';
import { AuthenticationService } from './services/authentication.service';

import { ButtonsModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { DatepickerModule } from 'ngx-bootstrap';

import { LiveComponent } from './live/live.component';
import { DailyPowerComponent } from './daily-power/daily-power.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InfoComponent } from './info/info.component';
import { PowerInfographicComponent } from './power-infographic/power-infographic.component';
import { PowerService } from './services/power.service';
import { PowergraphComponent } from './live/live.powergraph/powergraph.component';
import { DailyPowerGraphComponent } from './daily-power-graph/daily-power-graph.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    GraphsComponent,
    PowerGraphComponent,
    RegisterComponent,
    RegisterSuccessComponent,
    EmailVerifiedComponent,
    ResetPasswordComponent,
    RequestPasswordComponent,
    AlertComponent,
    LiveComponent,
    DailyPowerComponent,
    DashboardComponent,
    InfoComponent,
    PowerInfographicComponent,
    PowergraphComponent,
    DailyPowerGraphComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDropdownModule.forRoot(),
    // InputTextModule,
    // ButtonModule,
    // MessagesModule,
    // TabViewModule,
    ChartsModule,
    AccordionModule.forRoot(),
    DatepickerModule.forRoot()

  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    EmonClientService,
    AlertService,
    UpdateService,
    PowerService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

