/**
 * Created by dkroeske on 09/04/2017.
 */
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state : RouterStateSnapshot){
    if(localStorage.getItem('currentUser')) {
      return true;
    }

    // Redirect to login
    this.router.navigate([''], { queryParams: { returnUrl : state.url }});
    return false;
  }
}
