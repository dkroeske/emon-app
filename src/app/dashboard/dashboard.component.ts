import { Component, OnInit } from '@angular/core';

import { Logger } from '../models/logger';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loggers: Logger[] = [ new Logger('','Ds. Coolsmastraat 1', 2, 1), new Logger('','Stand 24', 2, 1)];


  constructor() { }

  ngOnInit() {
  }

}
