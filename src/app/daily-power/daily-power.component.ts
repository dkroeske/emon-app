import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { EmonClientService } from '../services/emon.client.service';
import { UpdateService } from '../services/update.service';
import * as moment from 'moment';
import Chart from 'chart.js';

@Component({
  selector: 'app-daily-power',
  templateUrl: './daily-power.component.html',
  styleUrls: ['./daily-power.component.css']
})

export class DailyPowerComponent implements OnInit {

  @Input() date = moment();

  private isLive = false;
  private lastUpdate = '--'

  // consumedPower: DailyPowerItem[] = [];
  // producedPower: DailyPowerItem[] = [];

  @ViewChild('canvas') canvasRef: ElementRef;

  //   {data: [], label: 'Max. opgenomen'},
  //   {data: [], label: 'Gem. opgenomen'},
  //   {data: [], label: 'Max. opgewekt'},
  //   {data: [], label: 'Gem. opgewekt'},


  private chart: any;

  //Chart config ...

  config: any = {
    type: 'line',

    data: {
      labels: [],

      datasets: [
        {
          label: 'Max consumed',
          // backgroundColor: 'rgba(211,211,211,0.0)',
          // borderColor: 'rgba(211,211,211,0.0)',
          backgroundColor: 'rgba(34,245,162,0.2)',
          borderColor: 'rgba(34,245,162,0.0)',
          data: [],
          fill: true,
          borderWidth: 2,
          pointRadius: 0
        },
        {
          label: 'Avg consumed',
          backgroundColor: 'rgba(34,245,162,0.0)',
          borderColor: 'rgba(34,245,162,1.0)',
          data: [],
          fill: false,
          borderWidth: 2,
          pointRadius: 0
        },
        {
          label: 'Max produced',
          // backgroundColor: 'rgba(211,211,211,0.2)',
          // borderColor: 'rgba(211,211,211,0.2)',
          backgroundColor: 'rgba(255,153,0,0.2)',
          borderColor: 'rgba(255,153,0,0.0)',
          data: [],
          fill: true,
          borderWidth: 0,
          pointRadius: 0
        },
        {
          label: 'Avg produced',
          backgroundColor: 'rgba(255,153,0,0.0)',
          borderColor: 'rgba(255,153,0,1.0)',
          data: [],
          fill: false,
          borderWidth: 2,
          pointRadius: 0
        }
      ]
    },

    options: {
      responsive: true,
      layout: {
        padding: {
          left: 5,
          right: 5,
          top: 5,
          bottom: 5
        }
      },
      elements: {
        point: {
        }
      },
      title: {
        display: false,
        text: 'Daily Power'
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            callback: function (label, index, labels) {
              const hour: number = Math.floor((index / 12));
              const minute: number = ((index * 5) % 60);
              const ftime = moment((hour + ':' + minute), 'HH:mm');
              const ftime_str = ftime.format('h:mm a');
              let retval = '';
              switch (index) {
                case 0:
                case 72:
                case 144:
                case 216:
                case 288 - 1:
                  retval = ftime_str;
                  break;
                default:
              }
              return retval;
            },
            stepSize: 1,
            min: 0,
            autoSkip: false,
            maxRotation: 0,
            minRotation: 0
          },
          scaleLabel: {
            display: false,
            labelString: 'Tijd'
          },
          gridLines: {
            display: false,
            offsetGridLines : false
          }
        }],
        yAxes: [{
          display: true,
          stacked: false,
          ticks: {
            callback: function(label, index, labels) {
              return Math.abs(label) + ' W';
            },
            beginAtZero: true
          },
          suggestedMin: 0,
          suggestedMax: 3600,
          scaleLabel: {
            display: false,
            labelString: 'Power [kW]'
          },
          gridLines: {
            display: false,
            offsetGridLines : false
          }
        }]
      },
      legend: {
        display: false,
      }
    }
  };


  // @ViewChild(BaseChartDirective) private _chart;
  //
  // // LineChart
  // public lineChartData: Array<any> = [
  //   {data: [], label: 'Max. opgenomen'},
  //   {data: [], label: 'Gem. opgenomen'},
  //   {data: [], label: 'Max. opgewekt'},
  //   {data: [], label: 'Gem. opgewekt'},
  // ];
  //
  // public lineChartLabels: Array<any> = [];
  //
  // public lineChartOptions: any = {
  //   hover: {
  //     onHover: function(event, active) {
  //     }
  //   },
  //   elements: {
  //     point: { radius: 0, hitRadius: 10, hoverRadius: 10 }
  //   },
  //   tooltips: {
  //     enabled: true,
  //     callbacks: {
  //       label: function (tooltipItem, data) {
  //
  //         // Get index
  //         const index = tooltipItem.index;
  //         const datasetIndex = tooltipItem.datasetIndex;
  //
  //         // Data
  //         const max_opgenomen = data.datasets[0].data[index];
  //         const gem_opgenomen = data.datasets[1].data[index];
  //         const max_opgewekt = data.datasets[2].data[index];
  //         const gem_opgewekt = data.datasets[3].data[index];
  //
  //         //
  //         let retval = '';
  //         switch (datasetIndex) {
  //           case 0:
  //             retval = 'Max. Opgenomen:' + max_opgenomen + ' W';
  //             break;
  //           case 1:
  //             retval = 'Gem. Opgenomen:' + gem_opgenomen + ' W';
  //             break;
  //           case 2:
  //             retval = 'Max. Opgewekt:' + max_opgewekt + ' W';
  //             break;
  //           case 3:
  //             retval = 'Gem. Opgewekt:' + gem_opgewekt + ' W';
  //             break;
  //         }
  //         return retval;
  //       },
  //
  //       title: function (tooltipItems, data) {
  //         return('Gemiddeld vermogen gedurende een dag');
  //       },
  //
  //       afterTitle: function (tooltipItems, data) {
  //         const index = tooltipItems[0].index;
  //         const hour: number = Math.floor((index / 12));
  //         const minute: number = ((index * 5) % 60);
  //         const ftime = moment((hour + ':' + minute), 'HH:mm');
  //         return( '@' + ftime.format('h:mm a') );
  //       }
  //     },
  //   },
  //   responsive: true,
  //   scales: {
  //     xAxes: [{
  //       ticks: {
  //         callback: function (label, index, labels) {
  //           const hour: number = Math.floor((index / 12));
  //           const minute: number = ((index * 5) % 60);
  //           const ftime = moment((hour + ':' + minute), 'HH:mm');
  //           const ftime_str = ftime.format('h:mm a');
  //           let retval = '';
  //           switch (index) {
  //             case 0:
  //             case 72:
  //             case 144:
  //             case 216:
  //             case 288 - 1:
  //               retval = ftime_str;
  //               break;
  //             default:
  //           }
  //           return retval;
  //         },
  //         stepSize: 1,
  //         min: 0,
  //         autoSkip: false,
  //         maxRotation: 0,
  //         minRotation: 0
  //       },
  //       scaleLabel: {
  //         display: true
  //       },
  //       gridLines: {
  //         display: false,
  //         offsetGridLines : false
  //       }
  //     }],
  //     yAxes: [
  //       {
  //         stacked: false,
  //         type: 'linear',
  //         position: 'left',
  //         id: 'y-axis-energy',
  //         ticks: {
  //           callback: function (label, index, labels) {
  //
  //             return Math.abs(label) + ' W';
  //           },
  //           beginAtZero : true
  //         },
  //         suggestedMin: 0,
  //         suggestedMax: 3600,
  //         scaleLabel: {
  //           display: true,
  //         },
  //         gridLines: {
  //           display: true,
  //           offsetGridLines: false
  //         }
  //       }
  //     ],
  //   },
  //   legend: {
  //     display: true,
  //     labels: {
  //       fontColor: 'rgb(0, 0, 0)'
  //     }
  //   }
  // };
  //
  // public lineChartColors: Array<any> = [
  //   { // Consumed Max
  //     backgroundColor: 'rgba(148,159,177,0.2)',
  //     borderColor: 'rgba(148,159,177,0.1)',
  //     // pointBackgroundColor: 'rgba(77,83,96,1)',
  //     // pointBorderColor: '#fff',
  //     // pointHoverBackgroundColor: '#fff',
  //     // pointHoverBorderColor: 'rgba(77,83,96,1)'
  //   },
  //   { // Consumed Mean
  //     backgroundColor: 'rgba(255,211,72,0.0)',
  //     borderColor: 'rgba(255,211,72,0.8)',
  //     // pointBackgroundColor: 'rgba(148,159,177,1)',
  //     // pointBorderColor: '#fff',
  //     // pointHoverBackgroundColor: '#fff',
  //     // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  //   },
  //   { // Produced Max
  //     backgroundColor: 'rgba(148,159,177,0.2)',
  //     borderColor: 'rgba(148,159,177,0.1)',
  //     // pointBackgroundColor: 'rgba(77,83,96,1)',
  //     // pointBorderColor: '#fff',
  //     // pointHoverBackgroundColor: '#fff',
  //     // pointHoverBorderColor: 'rgba(77,83,96,1)'
  //   },
  //   { // Produced Mean
  //     backgroundColor: 'rgba(34,245,162,0.0)',
  //     borderColor: 'rgba(34,245,162,0.8)',
  //     // pointBackgroundColor: 'rgba(148,159,177,1)',
  //     // pointBorderColor: '#fff',
  //     // pointHoverBackgroundColor: '#fff',
  //     // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  //   },
  // ];
  //
  // public lineChartLegend = true;
  // public lineChartType = 'line';

  //
  //
  //
  constructor(private emonClientService: EmonClientService,
              private updateService: UpdateService ) {
  }

  //
  //
  //
  ngOnInit() {

    // Setup chart
    this.chart = new Chart(this.canvasRef.nativeElement, {
      type: this.config.type,
      data: this.config.data,
      options: this.config.options
    });

    this.updateService.update_5min().subscribe( () => {
      this.getDaily();
    });
    this.getDaily();
  }

  //
  //
  //
  public getDaily(): void {

    const date = moment();
    const start = moment(date).startOf('day');
    const end = moment(date).endOf('day');

    const avg_consumed: number[] = new Array(288).fill(null)
    const max_consumed: number[] = new Array(288).fill(null);
    const avg_produced: number[] = new Array(288).fill(null);
    const max_produced: number[] = new Array(288).fill(null);
    const labels: number[] = new Array(288).fill(0);

    this.emonClientService.getTest(moment.utc(start), moment.utc(end))
      .subscribe(
        response => {

          try {
            const actual = response[response.length - 1];
            this.lastUpdate = moment(actual.created).format('HH:mm:ss a').toString();

            for (let idx = 0; idx < response.length; idx++) {

              // Result is in UTC, convert to LT using moment.js
              const dbtime = moment(response[idx].created).utc(true);
              const index = Math.floor(dbtime.hour() * 12 + dbtime.minute() / 5);

              try {
                avg_consumed[index] = -1000 * response[idx].p1[0].avg;
                max_consumed[index] = -1000 * response[idx].p1[0].max;
                avg_produced[index] = 1000 * response[idx].p1[1].avg;
                max_produced[index] = 1000 * response[idx].p1[1].max;

              } catch (ex) {
                console.log(ex.toString());
                this.isLive = false;
              }
            }

            this.isLive = true;

            console.log(max_consumed)

            this.config.data.datasets[0].data = max_consumed;
            this.config.data.datasets[1].data = avg_consumed;
            this.config.data.datasets[2].data = max_produced;
            this.config.data.datasets[3].data = avg_produced;
            this.config.data.labels = labels;

            this.chart.update();

            // // Update chars
            // this.lineChartLabels = xlabel;
            //
            // this.lineChartData[0].data = consumed_max;
            // this.lineChartData[1].data = consumed_mean;
            // this.lineChartData[2].data = produced_max;
            // this.lineChartData[3].data = produced_mean;

          } catch (ex) {
            this.isLive = false;
            console.log(ex);
          }
        },

        error => {
          this.isLive = false;
//          this.alertService.error(error.message);
        });
  }


  // Force reload op graph: https://github.com/valor-software/ng2-charts/issues/547
  private forceChartRefresh() {
    // setTimeout( () => {
    //   this._chart.refresh();
    // }, 10);
  }

  // chart events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}





// import { Component, OnInit } from '@angular/core';
//
// @Component({
//   selector: 'app-daily-power',
//   templateUrl: './daily-power.component.html',
//   styleUrls: ['./daily-power.component.css']
// })
// export class DailyPowerComponent implements OnInit {
//
//   constructor() { }
//
//   ngOnInit() {
//   }
//
// }
