import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPowerComponent } from './daily-power.component';

describe('DailyPowerComponent', () => {
  let component: DailyPowerComponent;
  let fixture: ComponentFixture<DailyPowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyPowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyPowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
