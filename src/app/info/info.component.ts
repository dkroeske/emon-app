import { Component, OnInit } from '@angular/core';
import { EmonClientService } from '../services/emon.client.service';
import { AlertService } from '../services/alert.service';
import { UpdateService } from '../services/update.service';
import {Logger} from '../models/logger';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})

export class InfoComponent implements OnInit {

  private isLive = false;
  private mfr: string;
  private id: string;
  private version: string;
  private equipment_id: string;
  private current_tariff: number;
  private power_failures: number;
  private long_power_failures: number;
  private logger_signature: string;
  private logger_description: string;
  private created: string;
  private lastupdate: string;

  //
  //
  //
  constructor(private emonClientService: EmonClientService,
              private updateService: UpdateService) {
  }

  //
  //
  //
  ngOnInit() {
    this.updateService.update_5min().subscribe(() => {
      this.getInfo();
    });
    this.getInfo();
  }

  //
  //
  //
  public getInfo(): void {

    this.emonClientService.getInfo(3)
      .subscribe(
        response => {
          try {

            this.isLive = true;
            this.mfr = response.p1.mfr;
            this.id = response.p1.id;
            this.version = response.p1.version;
            this.equipment_id = response.p1.equipment_id;
            this.current_tariff = response.p1.current_tariff;
            this.power_failures = response.p1.failures[0].power_failures;
            this.long_power_failures = response.p1.failures[1].long_power_failures;
            this.logger_description = response.logger.description;
            this.logger_signature = response.logger.signature;
            this.created = response.logger.created;
            this.lastupdate = response.logger.modified;

            //console.log(response);



          } catch (ex) {
            this.isLive = false;
          }
        },

        error => {
         this.isLive = false;
//          this.alertService.error(error.message);
        });
  }


}
