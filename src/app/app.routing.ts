import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home.component';
import { LoginComponent } from './components/login.component';
import { GraphsComponent } from './components/graphs.component';
import { PowerGraphComponent } from './components/power.graph.component';
import { RegisterComponent } from './components/register.component';
import { RegisterSuccessComponent } from './components/register.success.component';
import { EmailVerifiedComponent } from './components/email.verified.component';
import { ResetPasswordComponent } from './components/reset.password.component';
import { RequestPasswordComponent } from './components/request.password.component';
import { DashboardComponent } from './dashboard/dashboard.component';



import { AuthGuard } from './guards/auth.guard';


const appRoutes: Routes = [
  //{path: '', component: HomeComponent, canActivate: [AuthGuard] },
  {path: '', component: HomeComponent },
  {path: 'login', component: LoginComponent },
  {path: 'graphs', component: GraphsComponent, canActivate: [AuthGuard] },
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  {path: 'register', component : RegisterComponent },
  {
    path: 'registersuccess',
    component : RegisterSuccessComponent
  },
  {
    path: 'confirm',
    component : EmailVerifiedComponent
  },
  {
    path: 'reset-password',
    component : ResetPasswordComponent
  },
  {
    path: 'request-password',
    component : RequestPasswordComponent
  },

  {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);
