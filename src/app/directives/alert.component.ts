/**
 * Created by dkroeske on 09/04/2017.
 */
import { Component, OnInit, OnDestroy } from '@angular/core';

import { AlertService } from '../services/alert.service';

@Component({
  moduleId : module.id,
  selector : 'alert-component',
  templateUrl : 'alert.component.html'
})

export class AlertComponent implements OnInit, OnDestroy{
  message : string = "";
  type    : string = "";

  constructor( private alertService : AlertService) {}

  ngOnInit() {

    this.alertService.getMessage().subscribe( message => {

      if( message ) {
        switch ( message['type']) {
          case 'error':
            // this.msgs.push({severity:'error', summary:'Error', detail:message['text']});
            this.type="danger";
            this.message=message['text'];
            break;
          // case 'info':
          //   this.msgs.push({severity:'info', summary:'Info', detail:message['text']});
          //   break;
          // case 'success':
          //   this.msgs.push({severity:'success', summary:'Success', detail:message['text']});
          //   break;
        }
      }
    });
  }

  ngOnDestroy() {
  }
}
