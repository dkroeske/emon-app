/**
 * Created by dkroeske on 10/04/2017.
 */
import { Component, OnInit} from '@angular/core';
import { AlertService } from '../services/alert.service';

@Component({
  moduleId: module.id,
  selector: 'graphs-component',
  templateUrl: 'graphs.component.html',
  styles: [`

    /*.row [class*='!col-'] {*/
      /*background-color: #ffeeee;*/
      /*background-clip: content-box;*/
      /*min-height: 20px;*/
    /*}*/
    
    /*.panel-heading a:after {*/
      /*font-family:'Glyphicons Halflings';*/
      /*content:"\e114";*/
      /*float: right;*/
      /*color: grey;*/
    /*}*/
    /*.panel-heading a.collapsed:after {*/
      /*content:"\e080";*/
    /*}*/
    
  `]
})

export class GraphsComponent implements OnInit {

  name: string = 'Diederich';
  loggerName = 'Thuis'

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {
  }

};

