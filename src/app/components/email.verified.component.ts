/**
 * Created by dkroeske on 22/04/2017.
 */
import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../models/client';

@Component({
  moduleId : module.id,
  selector : 'EmailVerifiedComponent',
  template : `
    <div container>
      <div class="container">
        <div class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="margin-top: 50px">
          <alert type="success">
            <h4 class="alert-heading">Registratie voltooid</h4>
            <p>Email is geverified</p>
            <p class="mb-0"><strong>Klik op de link in de ontvangen email</strong> om de registratie te voltooien.</p>
          </alert>
        </div>
      </div>
    </div>
  `
})

export class EmailVerifiedComponent implements OnInit {

  constructor( private route : ActivatedRoute ) {

  }

  // onDone(value :any): void {
  //   console.log("onDone");
  //   this.router.navigate(['/']);
  // }
  private newClient: Client;

  ngOnInit(): void {
    // this.newClient = this.route.snapshot.data['id'];
    // console.log("RegisterSuccessComponent: " + this.newClient);
  }

}

