/**
 * Created by dkroeske on 15/04/2017.
 */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  moduleId: module.id,
  selector  : 'RegisterComponent',
  templateUrl: './register.component.html',
  styles: []
})

export class RegisterComponent {

  private userRegisterForm : FormGroup;

  constructor(
    private fb: FormBuilder,
    private authenticationService : AuthenticationService,
    private router : Router
  )
  {
    this.userRegisterForm = this.fb.group({
      'username': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    })
  }

  onSubmit(value: any) : void {

    this.authenticationService.register(value.username, value.email, value.password)
      .subscribe(

        user => {
          console.log(JSON.stringify(user));
          this.router.navigate(['/registersuccess']);
        },

        error => {
          console.log(error)
          // this.msgs = [];
          // this.msgs.push({severity:'error', summary:'Error Message', detail:err.message});

        });
  }

  onReset(): void {
    console.log("reset password");
  }

}
