/**
 * Created by dkroeske on 25/05/2017.
 */
import { Component, OnInit, ViewChild} from '@angular/core';
import { EmonClientService } from '../services/emon.client.service';
import { AlertService } from '../services/alert.service';
import { BaseChartDirective } from 'ng2-charts';
import * as moment from 'moment';

@Component({
  moduleId: module.id,
  selector: 'power-graph-component',
  templateUrl: 'power.graph.component.html'
})

export class PowerGraphComponent implements OnInit {

  @ViewChild(BaseChartDirective) private _chart;

  // LineChart
  public lineChartData: Array<any> = [
    {data: [], label: 'Consumed Mean'},
    {data: [], label: 'Consumed Max'},
    {data: [], label: 'Consumed Min'},
    {data: [], label: 'Produced Mean'},
    {data: [], label: 'Produced Max'},
    {data: [], label: 'Produced Min'}
  ];

  public lineChartLabels: Array<any> = [];

  public lineChartOptions: any = {
    hover: {
      onHover: function(event, active) {
      }
    },
      elements: {
        point: { radius: 0, hitRadius: 10, hoverRadius: 10 }
    },
    tooltips: {
      enabled: true,
      callbacks: {
        label: function (tooltipItem, data) {

          // Get index
          const index = tooltipItem.index;
          const datasetIndex = tooltipItem.datasetIndex;

          // Data
          const consumed_mean = data.datasets[0].data[index];
          const consumed_max = data.datasets[1].data[index];
          const consumed_min = data.datasets[1].data[index];
          const produced_mean = data.datasets[2].data[index];
          const produced_max = data.datasets[3].data[index];
          const produced_min = data.datasets[3].data[index];

          //
          let retval = '';
          switch (datasetIndex) {
            case 0:
              retval = 'Mean:' + consumed_mean + ' W';
              break;
            case 1:
              retval = 'Max: ' + consumed_max + ' W';
              break;
            case 2:
              retval = 'Min: ' + produced_mean + ' W';
              break;
            case 3:
              retval = 'Min: ' + produced_max + ' W';
              break;
          }
          return retval;
        },

        title: function (tooltipItems, data) {
          return('Vermogen [W]');
        },

        afterTitle: function (tooltipItems, data) {
          const index = tooltipItems[0].index;
          const hour: number = Math.floor((index / 12));
          const minute: number = ((index * 5) % 60);
          const ftime = moment((hour + ':' + minute), 'HH:mm');
          return( '@' + ftime.format('h:mm a') );
        }
      },
    },
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label, index, labels) {

            const hour: number = Math.floor((index / 12));
            const minute: number = ((index * 5) % 60);
            const ftime = moment((hour + ':' + minute), 'HH:mm');
            const ftime_str = ftime.format('h:mm a');
            let retval = '';
            switch (index) {
              case 0:
              case 72:
              case 144:
              case 216:
              case 288 - 1:
                retval = ftime_str;
                break;
              default:
            }
            return retval;
          },
          stepSize: 1,
          min: 0,
          autoSkip: false,
          maxRotation: 0,
          minRotation: 0
        },
        scaleLabel: {
          display: true
        },
        gridLines: {
          display: false,
          offsetGridLines : false
        }
      }],
      yAxes: [
        {
          stacked: false,
          type: 'linear',
          position: 'left',
          id: 'y-axis-energy',
          ticks: {
            callback: function (label, index, labels) {
              return label + ' W';
            }
          },
          suggestedMin: 0,
          suggestedMax: 1000,
          scaleLabel: {
            display: true,
          },
          gridLines: {
            display: true,
            offsetGridLines: false
          }
        }
      ],
    },
    legend: {
      display: true,
      labels: {
        fontColor: 'rgb(0, 0, 0)'
      }
    }
  };

  public lineChartColors: Array<any> = [
    { // Consumed Mean
      backgroundColor: 'rgba(255,211,72,0.8)',
      borderColor: 'rgba(255,211,72,0.8)',
      // pointBackgroundColor: 'rgba(148,159,177,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // Consumed Max
      backgroundColor: 'rgba(148,159,177,0.0)',
      borderColor: 'rgba(148,159,177,0.1)',
      // pointBackgroundColor: 'rgba(77,83,96,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    },

    { // Consumed Min
      backgroundColor: 'rgba(148,159,177,0.0)',
      borderColor: 'rgba(148,159,177,0.1)',
      // pointBackgroundColor: 'rgba(77,83,96,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    },

    { // Produced Mean
      backgroundColor: 'rgba(34,245,162,1.0)',
      borderColor: 'rgba(34,245,162,1.0)',
      // pointBackgroundColor: 'rgba(148,159,177,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // Produced Max
      backgroundColor: 'rgba(148,159,177,0.1)',
      borderColor: 'rgba(148,159,177,0.1)',
      // pointBackgroundColor: 'rgba(77,83,96,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // Min
      backgroundColor: 'rgba(148,159,177,0.1)',
      borderColor: 'rgba(148,159,177,0.1)',
      // pointBackgroundColor: 'rgba(77,83,96,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor(private emonClientService: EmonClientService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  //
  //
  //
  public getData(): void {

    let date = new Date("2017-05-30");

    const start = moment(date).startOf('day');
    const end = moment(date).endOf('day');

    // Debug
    console.log('start     :' + moment.utc(start).toString());
    console.log('end       :' + moment.utc(end).toString());

    const consumed: number[] = [];
    const produced: number[] = [];
    const xlabel: number[] = [];

    //
    // Server stores all datetime in UTC. Convert to UTC
    //
    this.emonClientService.getPower( moment.utc(start), moment.utc(end), true )
      .subscribe(
        response => {

          for (let idx = 0; idx < 288; idx++) {
            produced[idx] = 0;
            consumed[idx] = 0;
            xlabel[idx] = idx;
          }

          console.log(response);

          for (let idx = 0; idx < response.length; idx++) {

            // Result is in UTC, convert to LT using moment.js
            const dbtime = moment(response[idx].created).utc(true);
            const index = dbtime.hour() * 12 + Math.floor(dbtime.minute()) / 5;

            const datagram = response[idx].p1;

            try {
              consumed[index] = 1000 * response[idx].p1.power[0].value;
              produced[index] = 1000 * response[idx].p1.power[1].value;
            } catch (ex) {
              console.log(ex.toString());
            }
          }

          // console.log( 'consumed: ' + consumed);
          // console.log( 'produced: ' + produced);

          this.lineChartLabels = xlabel;
          this.lineChartData[0].data = consumed;
          this.lineChartData[1].data = produced;

          //
          this.forceChartRefresh();

        },

        error => {
          this.alertService.error(error.message);
        });
  }

  //
  //
  //
  public getTest(): void {

    let date = new Date("2017-06-03");

    const start = moment(date).startOf('day');
    const end = moment(date).endOf('day');

    // Debug
    console.log('start     :' + moment.utc(start).toString());
    console.log('end       :' + moment.utc(end).toString());

    const consumed_mean: number[] = [];
    const consumed_max: number[] = [];
    const consumed_min: number[] = [];

    const produced_mean: number[] = [];
    const produced_max: number[] = [];
    const produced_min: number[] = [];

    const xlabel: number[] = [];

    //
    // Server stores all datetime in UTC. Convert to UTC
    //
    this.emonClientService.getTest( moment.utc(start), moment.utc(end) )
      .subscribe(
        response => {

          // Every 5 minutes
          for (let idx = 0; idx < 288; idx++) {
            produced_mean[idx] = 0;
            produced_max[idx] = 0;
            produced_min[idx] = 0;

            consumed_mean[idx] = 0;
            consumed_max[idx] = 0;
            consumed_min[idx] = 0;

            xlabel[idx] = idx;
          }

          for (let idx = 0; idx < response.length; idx++) {

            console.log(response[idx])

            // Result is in UTC, convert to LT using moment.js
            const dbtime = moment(response[idx].created).utc(true);
            const index = Math.floor(dbtime.hour() * 12 + dbtime.minute() / 5);

            try {
              consumed_mean[index] = 1000 * response[idx].p1[0].avg;
              consumed_max[index] = 1000 * response[idx].p1[0].max;
              consumed_min[index] = 1000 * response[idx].p1[0].min;

              produced_mean[index] = -1000 * response[idx].p1[1].avg;
              produced_max[index] = -1000 * response[idx].p1[1].max;
              produced_min[index] = -1000 * response[idx].p1[1].min;

            } catch (ex) {
              console.log(ex.toString());
            }
          }

          // console.log( 'mean: ' + consumed_mean);
          // console.log( 'max : ' + consumed_max);
          // console.log( 'min : ' + consumed_min);

          this.lineChartLabels = xlabel;

          this.lineChartData[0].data = consumed_mean;
          this.lineChartData[1].data = consumed_max;
          this.lineChartData[2].data = consumed_min;

          this.lineChartData[3].data = produced_mean;
          this.lineChartData[4].data = produced_max;
          this.lineChartData[5].data = produced_min;


          //this.lineChartData[2].data = consumed_min;
//          this.lineChartData[1].data = produced;

          //
          this.forceChartRefresh();

        },

        error => {
          this.alertService.error(error.message);
        });
  }


  //
  // Force reload op graph: https://github.com/valor-software/ng2-charts/issues/547
  //
  private forceChartRefresh() {
    setTimeout( () => {
      this._chart.refresh();
    }, 10);
  }

  // chart events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

}
