/**
 * Created by dkroeske on 23/04/2017.
 */
/**
 * Created by dkroeske on 15/04/2017.
 */
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  moduleId: module.id,
  selector  : 'ResetPasswordComponent',
  templateUrl: './reset.password.component.html',
  styles: []
})

export class ResetPasswordComponent implements OnInit{

  private newPasswordForm : FormGroup;
  private accessToken;
  private userId;
  private email;

  constructor(
    private fb: FormBuilder,
    private authenticationService : AuthenticationService,
    private activatedRoute : ActivatedRoute
  )
  {
    this.newPasswordForm = this.fb.group({
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    })

    // Get params
    this.accessToken = this.activatedRoute.snapshot.queryParams['accesstoken'];
    this.userId = this.activatedRoute.snapshot.queryParams['userid'];

    console.log( "hoi: " + this.accessToken + ":" + this.userId);
  }

  ngOnInit() {
  }

  onSubmit(value: any) : void {

    this.authenticationService.resetPassword(value.password, this.accessToken, this.userId)
      .subscribe(

        user => {
          console.log(JSON.stringify(user));
          //this.router.navigate(['/registersuccess']);
        },

        error => {
          console.log(error)
          // this.msgs = [];
          // this.msgs.push({severity:'error', summary:'Error Message', detail:err.message});

        });
  }
}
