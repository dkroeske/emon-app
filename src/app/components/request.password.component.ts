/**
 * Created by dkroeske on 23/04/2017.
 */
/**
 * Created by dkroeske on 15/04/2017.
 */
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { AlertService } from '../services/alert.service';


@Component({
  moduleId: module.id,
  selector  : 'RequestPasswordComponent',
  templateUrl: './request.password.component.html',
  styles: []
})

export class RequestPasswordComponent implements OnInit{

  private requestPasswordForm : FormGroup;
  private email;

  constructor(
    private fb: FormBuilder,
    private authenticationService : AuthenticationService,
    private router : Router,
    private alertService : AlertService

)
  {
    this.requestPasswordForm = this.fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  ngOnInit() {
  }

  //
  //
  //
  onSubmit(event) : void {

    this.authenticationService.requestPassword(this.requestPasswordForm.value.email)
      .subscribe(

        user => {
          console.log(JSON.stringify(user));
          this.router.navigate(['/']);
        },

        error => {
          this.alertService.error("Email onbekend");
        });
  }
}
