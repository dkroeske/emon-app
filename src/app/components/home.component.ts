/**
 * Created by dkroeske on 09/04/2017.
 */
import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'HomeComponent',
  template: `
    <LoginComponent></LoginComponent>
   
    <video playsinline autoplay loop muted poster="../../assets/video/Dancing-Bulbs.jpg" id="bgvid">
      <source src="../../assets/video/Dancing-Bulbs.mp4" type="video/mp4"/>
      <source src="../../assets/video/Dancing-Bulbs.webm" type="video/webm"/>
    </video>
    
    <div id="emon">
      <h1><b>Jouw</b> slimme meter unlocked</h1>
      <p>Bouw je eigen DIY interface</p>
      <button>Meer info</button>
    </div>

    <!--<div class="container d-flex align-tem-center">-->
      <!--<div class="row">-->
        <!--<div class="col align-self-center">-->
          <!--<div class="emon">-->
            <!--<h1><b>Jouw</b><br>slimme meter<br>unlocked</h1>-->
            <!--<p>Bouw je eigen DIY interface</p>-->
            <!--<button>Meer info</button>-->
          <!--</div>-->
      <!---->
        <!--</div>-->
      <!--</div>-->
    <!--</div>-->
    `,

  styles: [`
    
    #bgvid {
      position: fixed;
      top: 50%;
      left: 50%;
      min-width: 100%;
      min-height: 100%;
      width: auto;
      z-index: -100;
      -webkit-transform: translateX(-50%) translateY(-50%);
      transform: translateX(-50%) translateY(-50%);
      background-size: cover;
    }
    
    #emon {
      font-family: 'Roboto', sans-serif;
      font-weight:400;
      background: rgba(0,0,0,0.3);
      color: white;
      padding: 2rem;
      width: 50%;
      margin: 0;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      font-size: 2rem;
    }
    h1 {
      font-size: 3rem;
      text-transform: uppercase;
      margin-top: 0;
      letter-spacing: .3rem;
    }
    
    /*!*.container h1 {*!*/
      /*!*color: white;*!*/
    /*!*}*!*/
    /*.emon {*/
      /*font-family: Agenda-Light, Agenda Light, Agenda, Arial Narrow, sans-serif;*/
      /*font-weight:100;*/
      /*background: rgba(0,0,0,0.3);*/
      /*color: white;*/
      /*padding: 2rem;*/
      /*!*width: 33%;*!*/
      /*margin: 2rem;*/
      /*!*float: right;*!*/
      /*font-size: 1.2rem;*/
    /*}*/
    
    /*.emon h1 {*/
      /*font-size: 4rem;*/
      /*text-transform: uppercase;*/
      /*margin-top: 0;*/
      /*letter-spacing: .3rem;*/
      /*text-align: center;*/
    /*}*/
    
    /*.emon p {*/
      /*font-size: 2rem;*/
      /*text-transform: uppercase;*/
      /*margin-top: 0;*/
      /*letter-spacing: .3rem;*/
      /*text-align: center;*/
    /*}*/
    
    /*.emon button {*/
      /*display: block;*/
      /*width: 80%;*/
      /*padding: .4rem;*/
      /*border: none;*/
      /*margin: 1rem auto;*/
      /*font-size: 2rem;*/
      /*background: rgba(255,255,255,0.23);*/
      /*color: #fff;*/
      /*border-radius: 3px;*/
      /*cursor: pointer;*/
      /*transition: .3s background;*/
    /*}*/
    /*.emon button:hover {*/
      /*background: rgba(0,0,0,0.5);*/
    /*}*/

    .note {
      position: absolute;
      z-index: 10;
      right: 0;
      top: 0;
      padding: 5px;
      background: #eee;
      max-width: 400px;
      border: 1px dotted #bbb;
    }

  `]
})

export class HomeComponent implements OnInit {

  private title = 'emon';
  private description = 'It is a long established fact that a reader will ' +
    'be distracted by the readable content of a page when looking at its layout. ' +
    'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of ' +
    'letters, as opposed to using \'Content here, content here\', making it look like ' +
    'readable English.';

  constructor() {
  }

  ngOnInit() {
    console.log("HomeComponent");
  }

  onclick() {
    this.title = "";
  }

}
