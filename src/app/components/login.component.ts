/**
 * Created by dkroeske on 08/04/2017.
 */

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { Client } from '../models/client';

@Component({
  moduleId  : module.id,
  selector  : 'LoginComponent',
  templateUrl: 'login.component.html',
  styles: []
})

export class LoginComponent implements OnInit {

  private loginForm: FormGroup;
  private message: string = '';

  constructor(
    private fb: FormBuilder,
    private authenticationService : AuthenticationService,
    private alertService : AlertService,
    private router : Router ) {
    this.loginForm = fb.group({
      'username': '',
      'password': ''
    });
  }

  ngOnInit(): void {
    this.authenticationService.logout();
  }

  login(value: any): void {
    this.authenticationService.login(value.username, value.password)
      .subscribe(

        user => {
          console.log("login:" + "click")
          this.router.navigate(['/dashboard']);
        },

        error => {
          console.log("login nok");
          this.alertService.error("Login failed");
        });
  }

  //
  //
  //
  register(): void {
    this.router.navigate(['/register']);
  }

  //
  // Reset password
  //
  requestPassword(): void {
    this.router.navigate(['/request-password']);
  }



}
