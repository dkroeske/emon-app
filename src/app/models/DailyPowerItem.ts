/**
 * Created by Gebruiker on 7/3/2017.
 */
export class DailyPowerItem {

  public max_value: number;
  public min_value: number;
  public avg_value: number;
  public timestamp: string;

  constructor(max_value: number, min_value: number, avg_value: number, timestamp: string) {
    this.timestamp = timestamp;
    this.max_value = max_value;
    this.min_value = min_value;
    this.avg_value = avg_value;
  }
}
