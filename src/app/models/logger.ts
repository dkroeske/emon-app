/**
 * Created by dkroeske on 12/04/2017.
 */
export class Logger {

  signature: string;
  description: string;
  id: Number;
  clientId: Number;

  constructor(signature: string, description: string, id: Number, clientId: Number) {
    this.signature = signature;
    this.description = description;
    this.id = id;
    this.clientId = clientId;
  }

}
