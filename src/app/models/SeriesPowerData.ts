/**
 * Created by Gebruiker on 7/2/2017.
 */
export class SeriesPowerData {

  public value: number;
  public timestamp: string;

  constructor(value: number, timestamp: string) {
    this.timestamp = timestamp;
    this.value = value;
  }

}
