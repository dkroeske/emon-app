import {Component, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';
import * as moment from 'moment';
import { BaseChartDirective } from 'ng2-charts';
import {Subscription} from 'rxjs/Subscription';
import {PowerService} from '../services/power.service';
import {SeriesPowerData} from '../models/SeriesPowerData';

@Component({
  selector: 'app-emon-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})

export class LiveComponent implements OnInit, OnDestroy {

  @Input() private powerSource = 0;

  private actualPower: string = '--';

  private prev: number = 0;
  private cur: number = 0;
  private delta: string = '--';

  private created: string = '';
  private isLive = false;
  private lastUpdate = '';

  graphdata: SeriesPowerData[] = [];

  private powerSubscription: Subscription;

  //
  //
  //
  constructor(private powerService: PowerService) {}

  //
  //
  //
  ngOnInit() {
    this.powerSubscription = this.powerService.getCurrentPower()
      .subscribe(
        response => {
          this.handleCurrentPower(response);
        },
        error => console.log(error)
      );
  }

  //
  //
  //
  ngOnDestroy() {
    this.powerSubscription.unsubscribe();
  }

  //
  //
  //
  private handleCurrentPower(response: any): void {
    try {
      const actual = response[response.length - 1];

      switch (this.powerSource) {
        case 0:
        case 1:

          this.cur = actual.p1.power[this.powerSource].value * 1000.0;
            const d: number = (100.0 * ((this.cur - this.prev) / this.prev))
            if (d !== d ) {
              this.delta = '...';
            } else {
              if (d > 0.0) {
                this.delta = '△' + Math.abs(d).toFixed(1) + '%';
              } else {
                if (d < 0.0) {
                  this.delta = '▽' + Math.abs(d).toFixed(1) + '%';
                } else {
                  this.delta = '◇' + Math.abs(d).toFixed(1) + '%';
                }
              }
            }
          this.prev = this.cur;

          this.actualPower = (actual.p1.power[this.powerSource].value * 1000).toFixed(0).toString();
          this.created = moment(actual.created).format('HH:mm:ss a').toString();
          this.lastUpdate = this.created;

          this.graphdata = [];
          for (let idx = 0; idx < response.length; idx++) {
            this.graphdata.push(
              new SeriesPowerData(
                response[idx].p1.power[this.powerSource].value * 1000,
                response[idx].created
              )
            );
          }
          this.isLive = true;
          break;

        // case 1:
        //
        //   //
        //
        //   this.cur = actual.p1.power[1].value * 1000.0;
        //   this.delta = (this.prev - this.cur).toFixed(0);
        //   this.prev = this.cur;
        //
        //   //
        //   this.actualPower = (actual.p1.power[1].value * 1000).toFixed(0).toString();
        //   this.created = moment(actual.created).format('HH:mm:ss a').toString();
        //   this.lastUpdate = this.created;
        //
        //   this.graphdata = [];
        //   for (let idx = 0; idx < response.length; idx++) {
        //     this.graphdata.push(
        //       new SeriesPowerData(
        //         response[idx].p1.power[1].value * 1000,
        //         response[idx].created
        //       )
        //     );
        //   }
        //   this.isLive = true;
        //   break;

        case 2:
          this.actualPower = (actual.s0.value * 1000).toFixed(0).toString();
          this.created = moment(actual.created).format('HH:mm:ss a').toString();
          this.lastUpdate = this.created;
          // for (let idx = 0; idx < response.length; idx++) {
          //   powerSerieData.push((response[idx].s0.value * 1000).toFixed(0));
          //   xLabel.push( moment(response[idx].created).format('HH:mm a').toString() );
          // }

          this.isLive = true;
          break;

        case 3:
          this.actualPower = (actual.s0.value * 1000).toFixed(0).toString();
          this.created = moment(actual.created).format('HH:mm:ss a').toString();
          this.lastUpdate = this.created;
          // for (let idx = 0; idx < response.length; idx++) {
          //   powerSerieData.push((response[idx].s0.value * 1000).toFixed(0));
          //   xLabel.push( moment(response[idx].created).format('HH:mm a').toString() );
          // }
          this.isLive = true;
          break;

        default:
          break;
      }
    } catch (ex) {
      console.log(ex)
      this.isLive = false;
    }
  }
}
