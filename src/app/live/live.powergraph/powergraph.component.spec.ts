import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowergraphComponent } from './powergraph.component';

describe('PowergraphComponent', () => {
  let component: PowergraphComponent;
  let fixture: ComponentFixture<PowergraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowergraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowergraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
