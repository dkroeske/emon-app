import { Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams} from '@angular/http';

import * as moment from 'moment';
import { TimerObservable } from 'rxjs/Observable/TimerObservable';
import { Subscription} from 'rxjs/Subscription';

import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Subject} from "rxjs/Subject";

@Injectable()
export class PowerService {

  private baseUrl = 'http://192.168.2.25:8080/api/v1';

  //
  //
  //
  constructor(private http: Http) {
  }

  //
  //
  //
  getCurrentPower(): Observable<any> {

    return TimerObservable.create(1000, 10000).flatMap(() => {

      const now = moment.utc();
      const startDate = moment(now).subtract(5, 'm');
      const endDate = moment(now);
      const decoded = true;

      const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
      const loggerId = 3;
      const url = this.baseUrl + '/loggers/' + loggerId + '/power';

      const params = new URLSearchParams();
      params.set('filter', '');
      params.set('startDate', startDate.toString());
      params.set('endDate', endDate.toString());
      params.set('decoded', decoded.toString());
      params.set('access_token', access_token);

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      const options = new RequestOptions({headers: headers, search: params});

      return this.http
        .get(url, options)
        .map( (res: Response) => {
          return res.json();
        })
        .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
    });
  }

}
