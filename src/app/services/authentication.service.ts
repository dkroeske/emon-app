/**
 * Created by dkroeske on 09/04/2017.
 */

import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Client } from '../models/client';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/throw'

@Injectable()
export class AuthenticationService {

  private url = 'http://192.168.2.25:8080/api/v1/clients';
  private headers = new Headers();

  constructor( private http: Http ){
    this.headers.append('Content-Type', 'application/json');
  }


  //
  // Log in
  //
  login(username: string, password: string) : Observable<Client> {
    return this.http.post(this.url + "/login",
      JSON.stringify({username:username, password:password}),
      new RequestOptions({headers: this.headers}))
      .map( (res: Response) => {
        const user = res.json();
        if (user && user.id) {
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;
      })
      .catch( (error:any) => Observable.throw(error.json().error || 'Server error') );
  }

  //
  // Register new user
  //
  register(username: string, email: string, password: string) :  Observable<Client> {
    return this.http.post(this.url, JSON.stringify({username:username, email:email, password:password}),
      new RequestOptions({headers: this.headers}))
      .map( (res: Response) => {
        let user = res.json();
        return user;
      })
      .catch( (error:any) => Observable.throw(error.json().error || 'Server error') );
  }

  //
  // Request password change
  //
  resetPassword(password: string, accessToken: string, userId: string) : Observable<Client> {

    let params : URLSearchParams = new URLSearchParams();
    params.set('access_token', accessToken);
    let requestOptions = new RequestOptions();
    requestOptions.search = params;
    requestOptions.headers = this.headers;

    let url = this.url + '/' + userId;
    console.log(params.toString())
    return this.http.patch(url, JSON.stringify({password:password}), requestOptions)
      .map( (res: Response) => {
        let user = res.json();
        return user;
      })
      .catch( (error:any) => Observable.throw(error.json().error || 'Server error') );
  }

  //
  // Request password change
  //
  requestPassword(email : string) : Observable<Client> {

    let requestOptions = new RequestOptions();
    requestOptions.headers = this.headers;

    let url = this.url + '/reset'
    return this.http.post(url, JSON.stringify({email:email}), requestOptions)
      .map( (res: Response) => {
        let user = res.json();
        return user;
      })
      .catch( (error:any) => Observable.throw(error.json().error || 'Server error') );
  }


  logout() {
    localStorage.removeItem('currentUser');
  }

}
