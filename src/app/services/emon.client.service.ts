/**
 * Created by dkroeske on 08/04/2017.
 */
import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Moment } from 'moment';

@Injectable()
export class EmonClientService {

  private baseUrl = 'http://192.168.2.25:8080/api/v1';
  private headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  getLoggers() {
    const userId = JSON.parse(localStorage.getItem('currentUser'))['userId'];
    const url = this.baseUrl + '/clients/' + userId + '/loggers';
    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    const params = new URLSearchParams();
    params.set('access_token', access_token);
    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http.get(url, options)
      .map( (response: Response) => {
        const loggers = response.json();
        localStorage.setItem('loggers', JSON.stringify(loggers));
      });
  }

  //
  //
  //
  getEnergyData(date: Date = new Date(),
                mode: string = '',
                decoded: boolean = false,
                filter?: string): Observable<any> {

    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    const loggerId = 3;
    const url = this.baseUrl + '/loggers/' + loggerId + '/energy/' + mode;

    const params = new URLSearchParams();

    if (filter) { params.set('filter', filter); }
    if (date) { params.set('date', date.toString()); }
    if (decoded) { params.set('decoded', decoded.toString()); }

    params.set('access_token', access_token);
    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http
      .get(url, options)
      .map( (response: Response) => {
        return response.json();
      })
      .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
  }

  //
  //
  //
  getPower(startDate: Moment,
           endDate: Moment,
           decoded: boolean = false,
           filter?: string): Observable<any> {
    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    const loggerId = 3;
    const url = this.baseUrl + '/loggers/' + loggerId + '/power';

    const params = new URLSearchParams();

    if (filter) { params.set('filter', filter); }
    if (startDate) { params.set('startDate', startDate.toString()); }
    if (endDate) { params.set('endDate', endDate.toString()); }
    if (decoded) { params.set('decoded', decoded.toString()); }

    params.set('access_token', access_token);
    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http.get(url, options)
      .map( (response: Response) => {
        return response.json();
      })
      .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
  }

  //
  //
  //
  getInfo(loggerId: number, filter?: string): Observable<any> {

    const url = this.baseUrl + '/loggers/' + loggerId + '/info';

    const params = new URLSearchParams();
    if (filter) { params.set('filter', filter); }
    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    params.set('access_token', access_token);

    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http.get(url, options)
      .map( (response: Response) => {
        return response.json();
      })
      .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
  }

  //
  //
  //
  getTest(startDate: Moment,
           endDate: Moment,
           filter?: string): Observable<any> {
    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    const loggerId = 3;
    const url = this.baseUrl + '/loggers/' + loggerId + '/test';

    const params = new URLSearchParams();

    if (filter) { params.set('filter', filter); }
    if (startDate) { params.set('startDate', startDate.toString()); }
    if (endDate) { params.set('endDate', endDate.toString()); }

    params.set('access_token', access_token);
    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http.get(url, options)
      .map( (response: Response) => {
        return response.json();
      })
      .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
  }

  //
  //
  //
  getPowerDecoded( startDate: Moment, endDate: Moment, interval: number, filter?: string): Observable<any> {
    const access_token = JSON.parse(localStorage.getItem('currentUser'))['id'];
    const loggerId = 3;
    const url = this.baseUrl + '/loggers/' + loggerId + '/test';

    const params = new URLSearchParams();

    if (filter) { params.set('filter', filter); }
    if (startDate) { params.set('startDate', startDate.toString()); }
    if (endDate) { params.set('endDate', endDate.toString()); }
    if (interval) { params.set('interval', interval.toString()); }


    params.set('access_token', access_token);
    const options = new RequestOptions({headers: this.headers, search: params});

    return this.http.get(url, options)
      .map( (response: Response) => {
        return response.json();
      })
      .catch( (error: any) => Observable.throw(error.json().error || 'server error' ));
  }
}
