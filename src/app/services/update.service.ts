/**
 * Created by Gebruiker on 6/5/2017.
 */
import { Injectable } from '@angular/core';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { EmonClientService } from '../services/emon.client.service';
import * as moment from 'moment';
import {observable} from "rxjs/symbol/observable";

@Injectable()

export class UpdateService {

  constructor(emonClientService: EmonClientService) {}

  update_10s = () => {
    return IntervalObservable
      .create(10 * 1000);
  }

  update_5min = () => {
    return IntervalObservable
      .create(5 * 60 * 1000);
  }
}
