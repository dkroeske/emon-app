import {Component, ElementRef, OnInit, OnDestroy, ViewChild, AfterViewInit, NgZone, Input} from '@angular/core';
import {TimerObservable} from 'rxjs/Observable/TimerObservable';
import {Subscription} from 'rxjs/Subscription';
import * as moment from 'moment';
import { EmonClientService } from '../services/emon.client.service';
import { UpdateService } from '../services/update.service';
import {PowerService} from '../services/power.service';

@Component({
  selector: 'app-power-infographic',
  templateUrl: './power-infographic.component.html',
  styleUrls: ['./power-infographic.component.css']
})

export class PowerInfographicComponent implements OnInit, AfterViewInit {

  // Ref. to Canvas
  @ViewChild('powerInfoGraphic') canvasRef: ElementRef;

  // Ref. to images
  @ViewChild('img_home_orange') img_home_orange: ElementRef;
  @ViewChild('img_home_green') img_home_green: ElementRef;

  power: number = 0;

  private running: boolean;
  private updateCnt = 0;
  private timerSubscription: Subscription;
  private powerSubscription: Subscription;

  constructor(private ngZone: NgZone,
              private powerService: PowerService) {
  }

  ngAfterViewInit() {
  }

  // onResize(event) {
  //   console.log(event)
  // }

  ngOnDestroy() {
    this.running = false;
    this.timerSubscription.unsubscribe();
    this.powerSubscription.unsubscribe();
  }

  ngOnInit() {
    this.running = true;

    const timer = TimerObservable.create(1000, 500);
    this.timerSubscription = timer.subscribe(t => {
      this.update(t);
    });

    this.powerSubscription = this.powerService.getCurrentPower()
      .subscribe(
        response => {
          try {
            const actual = response[response.length - 1];
            const consumed = (actual.p1.power[0].value * 1000);
            const produced = (actual.p1.power[1].value * 1000);
            this.power = (produced - consumed);
          } catch (ex) {
            console.log(ex);
          }
        },
        error => console.log(error)
      );

    // Start
    this.paint();

  }

  //
  public update(t: number) {
    requestAnimationFrame(() => this.paint());
  }

  private paint_a() {
    if (!this.running) {
      return;
    }

    const ctx: CanvasRenderingContext2D = this.canvasRef.nativeElement.getContext('2d');

    const width = this.canvasRef.nativeElement.width;
    const height = this.canvasRef.nativeElement.height;
    const midWidth = width / 2.0;
    const midHeight = height / 2.0;

    ctx.clearRect(0.0, 0.0, width, height);

    ctx.beginPath();
    ctx.moveTo(0, midHeight / 2.0);
    ctx.quadraticCurveTo(midWidth, midHeight, width, 0);
    ctx.quadraticCurveTo(width - 25, 25, width, midHeight);
    ctx.quadraticCurveTo(midWidth, midHeight, 0, height);
    ctx.quadraticCurveTo(25, midHeight, 0, midHeight / 2.0);
    ctx.closePath();
    ctx.stroke();



  }


  //
  private paint() {

    if (!this.running) {
      return;
    }

    const ctx: CanvasRenderingContext2D = this.canvasRef.nativeElement.getContext('2d');

    const width = this.canvasRef.nativeElement.width;
    const height = this.canvasRef.nativeElement.height;
    const midWidth = width / 2.0;
    const midHeight = height / 2.0;

    //console.log('Canvas (w:h)=(' + width + ',' + height + ')');
    ctx.clearRect(0.0, 0.0, width, height);

    ctx.font = '24px serif';

    if (this.power >= 0 ) {

      ctx.drawImage(this.img_home_green.nativeElement, (width / 2.0 - 75.0 / 2), (height / 2.0 - 75.0 / 2), 75, 75);
      ctx.fillText(Math.abs(this.power).toFixed(0) + ' W leveren', 75.0, height - 15);

      ctx.save();
      ctx.translate(width, 0.0);
      ctx.scale(-1.0, 1.0);
      ctx.translate(width / 2.0 + 40, (height / 2.0) - 30);

      ctx.save();
      for (let idx = 0; idx < 4; idx++) {
        this.drawArrow(ctx, 'rgba(0, 255, 0, 0.1)');
        ctx.translate(25, 0);
      }
      ctx.restore();

      for (let idx = 0; idx < 4; idx++) {
        ctx.save();
        ctx.translate(100 + ((this.updateCnt - 2 - idx) * 25), 0.0);
        const alpha = 1.0 - 0.4 * idx;
        this.drawArrow(ctx, 'rgba(0, 255, 0, ' + alpha + ')');
        ctx.restore();
      }
      ctx.restore();
    } else {

//      ctx.drawImage(this.img_home_orange.nativeElement, (width / 2.0 - 75.0 / 2), (height / 2.0 - 75.0 / 2), 75, 75);
      ctx.drawImage(this.img_home_orange.nativeElement, (width / 2.0 - 25), (height / 2.0 - 25), 50, 50);

      ctx.fillText(Math.abs(this.power).toFixed(0) + ' W opnemen', 75.0, height - 15);

      ctx.save();
      ctx.translate(0, (height / 2.0) - 30);
      for (let idx = 0; idx < 4; idx++) {
        this.drawArrow(ctx, 'rgba(255, 128, 0, 0.1)');
        ctx.translate(25, 0);
      }
      ctx.restore();

      for (let idx = 0; idx < 4; idx++) {
        ctx.save();
        ctx.translate((this.updateCnt - 2 - idx) * 25, (height / 2.0) - 30);
        const alpha = 1.0 - 0.4 * idx;
        this.drawArrow(ctx, 'rgba(255, 128, 0, ' + alpha + ')');
        ctx.restore();
      }
    }

    this.updateCnt++;
    this.updateCnt %= 6;

  }

  private drawArrow(ctx: CanvasRenderingContext2D, fillStyle: string) {
    const arrowWidth = 30.0;
    const arrowHeight = 40.0;
    const arrowMidHeight = arrowHeight / 2.0;
    const arrowIn = 10;

    ctx.beginPath()
    ctx.moveTo(0.0, 0.0);
    ctx.lineTo(arrowWidth - arrowIn, 0.0);
    ctx.lineTo(arrowWidth, arrowMidHeight);
    ctx.lineTo(arrowWidth - arrowIn, arrowHeight);
    ctx.lineTo(0.0, arrowHeight);
    ctx.lineTo(0.0 + arrowIn, arrowMidHeight);
    ctx.closePath();
    ctx.fillStyle = fillStyle;
    ctx.fill();
  }
}

