import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerInfographicComponent } from './power-infographic.component';

describe('PowerInfographicComponent', () => {
  let component: PowerInfographicComponent;
  let fixture: ComponentFixture<PowerInfographicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerInfographicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerInfographicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
