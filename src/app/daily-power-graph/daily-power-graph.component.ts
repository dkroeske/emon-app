import {Component, ElementRef, OnInit, OnChanges, SimpleChanges, ViewChild, OnDestroy, Input} from '@angular/core';
import Chart from 'chart.js';
import * as moment from 'moment';
import {SeriesPowerData} from '../models/SeriesPowerData';

@Component({
  selector: 'app-daily-power-graph',
  templateUrl: './daily-power-graph.component.html',
  styleUrls: ['./daily-power-graph.component.css']
})

export class DailyPowerGraphComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('canvas') canvasRef: ElementRef;
  private chart: any;

  // Chart config ...
  config: any = {
    type: 'line',

    data: {
      labels: [],

      datasets: [
        {
          label: 'single dot',
          pointBackgroundColor: 'rgba(255,0,0,0.8)',
          pointRadius: 3
        },
        {
          label: 'PowerData',
          backgroundColor: 'rgba(211,211,211,0.2)',
          borderColor: 'rgba(211,211,211,0.8)',
          data: [],
          fill: false,
          borderWidth: 2,
          pointRadius: 0
        }
      ]
    },

    options: {
      responsive: true,
      layout: {
        padding: {
          left: 5,
          right: 5,
          top: 5,
          bottom: 5
        }
      },
      elements: {
        point: {
        }
      },
      title: {
        display: false,
        text: 'Daily Power'
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: false,
          ticks: {
            callback: function (label, index, labels) {
              let retval = '';
              switch (index) {
                case 0:
                // case labels.length / 3:
                // case (2 * labels.length) / 3:
                case labels.length - 1:
                  retval = label;
                  break;
                default:
              }
              return retval;
            },
            stepSize: 1,
            min: 0,
            autoSkip: false,
            maxRotation: 0,
            minRotation: 0
          },
          scaleLabel: {
            display: false,
            labelString: 'Tijd'
          },
          gridLines: {
            display: false,
            offsetGridLines : false
          }
        }],
        yAxes: [{
          display: false,
          ticks: {
            callback: function(label, index, labels) {
              let retval = '';
              switch (index) {
                case 0:
                  retval = (label / 1000.0).toFixed(1) + ' kW';
                  break;
                default:
              }
              return retval;
            },
            beginAtZero: true
          },
          suggestedMin: 0,
          suggestedMax: 3600,
          scaleLabel: {
            display: false,
            labelString: 'Power [kW]'
          },
          gridLines: {
            display: false,
            offsetGridLines : false
          }
        }]
      },
      legend: {
        display: false,
      }
    }
  };

  //
  //
  //
  constructor(private elementRef: ElementRef) { }

  //
  //
  //
  ngOnInit() {
    // Setup chart
    this.chart = new Chart(this.canvasRef.nativeElement, {
      type: this.config.type,
      data: this.config.data,
      options: this.config.options
    });
  }

  //
  //
  //
  ngOnDestroy() {
  }

  //
  //
  //
  ngOnChanges(changes: SimpleChanges) {
    // if (this.chart && changes['series']) {
    //   this.handleCurrentPower(this.series);
    // }
  }

  private handleCurrentPower(series): void {

    const x = [];
    const y = [];

    try {
      for (let idx = 0; idx < series.length; idx++) {
        x.push( moment(series[idx].timestamp).format('HH:mm a').toString() );
        y.push( series[idx].value.toFixed(0));
      }
      this.config.data.datasets[1].data = y;
      this.config.data.labels = x;

      // Generate single dot data
      const dummy = [].fill(null, 0, series.length);
      dummy[series.length - 1] = y[series.length - 1];
      this.config.data.datasets[0].data = dummy;

      this.chart.update();
    } catch (ex) {
      console.log(ex);
    }
  }

}

