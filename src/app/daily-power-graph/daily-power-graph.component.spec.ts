import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPowerGraphComponent } from './daily-power-graph.component';

describe('DailyPowerGraphComponent', () => {
  let component: DailyPowerGraphComponent;
  let fixture: ComponentFixture<DailyPowerGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyPowerGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyPowerGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
